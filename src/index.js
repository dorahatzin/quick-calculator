/**
 * Create a quick calculator
 */
import './style.less';
import axios from 'axios';

class Calculator {

    constructor() {
        this.screenCalc = document.querySelector(".calculation");
        this.screenResult = document.querySelector(".result");
        const buttons = document.querySelector(".buttons");

        let canOperate = false;
        let arrayOfNumbers = [];

        buttons.addEventListener("click", (e) => {
            // fat arrow function so we can access reset and calculate functions
            let calcValue = this.screenCalc.innerHTML;
            let buttonTarget = e.target;

            if (buttonTarget.className.indexOf("allClear") > -1) {
                this.reset();
            }
            else if (buttonTarget.className.indexOf("save") > -1) {
                // parse the result,date and browser
                this.save(this.screenResult.innerHTML, new Date(), window.navigator.userAgent);
            } else if (buttonTarget.className.indexOf("number") > -1) {
                canOperate = true;
                this.screenCalc.innerHTML = calcValue + buttonTarget.innerHTML;

                // this is to prevent the user from clicking on the dot multiple times
                this.detectTwoOfTheSameCharacters(this.screenCalc.innerHTML);

            } else if (buttonTarget.className.indexOf("operator") > -1) {

                if (canOperate) {

                    this.screenCalc.innerHTML = calcValue + buttonTarget.innerHTML;

                    if (buttonTarget.innerHTML === '=') {
                        // get the full calculation and remove the "="
                        let fullCalculation = this.screenCalc.innerHTML.substring(0, this.screenCalc.innerHTML.length - 1);
                        let reg = /[^0123456789.]/g;
                        let matchOp = fullCalculation.match(reg); // get the operator using RegEx
                        // get the array of the 2 numbers
                        arrayOfNumbers = fullCalculation.split(matchOp[0]);
                        this.screenResult.innerHTML = this.calculate(arrayOfNumbers[0], arrayOfNumbers[1], matchOp[0]);

                    }
                }
                canOperate = false;
            }

        }, false);
    }

    /**
     * Dont let 2 of the same characters in the same string
     * @param {String} str 
     */
    detectTwoOfTheSameCharacters(str) {

        let reg = /[^0123456789]/g;
        let match = str.match(reg);

        if (match && match.length > 1) {
            this.screenCalc.innerHTML = this.screenCalc.innerHTML.substring(0, this.screenCalc.innerHTML.length - 1);
        }
    }

    /**
     * Make the calculations between the 2 numbers
     * @param {String} firstNum 
     * @param {String} secondNum 
     * @param {String} operator 
     */
    calculate(firstNum, secondNum, operator) {
        switch (operator) {
            case "+":
                return parseFloat(firstNum) + parseFloat(secondNum);
            case "-":
                return parseFloat(firstNum) - parseFloat(secondNum);
            case "x":
                return parseFloat(firstNum) * parseFloat(secondNum);
            case "÷":
                return parseFloat(firstNum) / parseFloat(secondNum);

        }
    }

    /**
     * Reset the Display
     */
    reset() {
        this.screenCalc.innerHTML = "";
        this.screenResult.innerHTML = "";
    }

    /**
     * Save the data
     * @param {String} str 
     */
    save(sum, date, browser) {
        const Url = 'http://localhost:8000/data.php';
        axios({
            method: 'post',
            url: Url,
            data: {
                'sum': sum,
                'IPAddress': '1.1.1.1',
                'date': date,
                'browser': browser
            }
        }).then(data => console.log(data)).catch(err => console.log("test ", err))
    }

}

new Calculator();