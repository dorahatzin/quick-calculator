<?php

header("Access-Control-Allow-Origin: *");

$data = json_decode(file_get_contents("php://input"));
 
// make sure data is not empty
if(
    !empty($data->sum) &&
    !empty($data->IPAddress) &&
    !empty($data->date) &&
    !empty($data->browser)
)

$sum = $data->sum;
$IPAddress = $data->IPAddress;
$date = $data->date;
$browser = $data->browser;

$list = array
(
   $sum,$IPAddress,$date,$browser    
);

$file = fopen("data.csv","w");

foreach ($list as $line)
{
  fputcsv($file,explode(',',$line));
}

fclose($file); 

$response_data = array('message' => "success");
header('Content-Type: application/json');

http_response_code(200);
echo json_encode($response_data);
?>